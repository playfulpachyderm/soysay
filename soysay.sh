#!/bin/bash

if [[ -t 0 ]]; then
    # soysay was run directly; `-n` flag is not allowed
    result=$(cowsay -f you_will_soyjak $@)
else
    # something is piped to soysay; preserve spacing using `-n`
    result=$(cowsay -n -f you_will_soyjak $@)
fi

# Add a blank line before printing the output, to ensure that the bubble always starts
# at the beginning of the line (prevent the bash prompt from interfering)
echo "
${result}"
