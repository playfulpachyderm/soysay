#!/bin/bash

RED="\e[31m"
GREEN="\e[32m"
ENDCOLOR="\e[0m"

# Ensure cowsay is installed
#
if ! which cowsay >/dev/null
then
	echo -e "${RED}No \`cowsay\` executable detected.${ENDCOLOR}"
	echo -e "${RED}\`soysay\` will not work without cowsay installed.${ENDCOLOR}"
	echo ""

	case "$(uname -s)" in
	    Linux*)
			echo "I think your platform is Linux."
			echo "You can probably install cowsay using your system's package manager."
			echo "e.g. (if you are on Ubuntu):"
			echo ""
			echo "          apt install cowsay"
			echo ""
			echo "If you're not on Ubuntu, you probably know what your package manager is."
			;;
	    Darwin*)
			echo "I think your platform is Mac."
			echo "You can install cowsay using \`brew\`:"
			echo ""
			echo "          brew install cowsay"
			;;
	    CYGWIN* | MINGW*)
			echo "I think your platform is Windows (either Cygwin or MinGW)."
			echo "I don't know how you install cowsay on your platform.  Try googling it I guess :("
			;;
	    *)          
			echo "I can't tell what platform you are on."
			echo "Try googling 'install cowsay on <your platform>'."
			;;
	esac
	echo -e "${RED}ERROR: soysay was NOT installed.${ENDCOLOR}"
	exit 1
fi

echo "Installing soysay..."

cp soyjaks/* /usr/share/cows

cp soysay.sh /usr/local/bin
ln -s /usr/local/bin/soysay.sh /usr/local/bin/soysay
chmod +x /usr/local/bin/soysay*

echo -e "${GREEN}soysay has been installed!${ENDCOLOR}"
echo "Soyjak cowfiles have been installed to /usr/share/cows."
echo "soysay is installed to /usr/local/bin."
echo "If you decide you don't want soysay anymore, you can uninstall it by running the \`uninstall.sh\` script that came with this installer."
echo ""
echo "Try it out!  e.g.:"
echo ""
echo "          soysay \"You WILL live in the pod.  You WILL eat the bugs.  You will own NOTHING, and you will be happy!\""
echo ""
echo -e "${GREEN}Enjoy!${ENDCOLOR}"
