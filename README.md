# Soysay

This is a cowsay extension that provides soyjak cowfiles.

## Installation

Run:

```sh
# Clone the repo
git clone https://gitlab.com/playfulpachyderm/soysay.git

# Run the install script
cd soysay && ./install.sh

# Or you can manually copy the `soysay.sh` script and the soyjaks to your favorite
# place in your $PATH (this is all the installer does).
```

If cowsay is not installed, it will prompt you to install cowsay first, since otherwise soysay won't work.

## Usage

Once soysay is installed, you can use it the same way as cowsay. e.g.,
```sh
soysay "You WILL live in the pod.  You WILL eat the bugs.  You will own NOTHING, and you will be happy!"
```
![Output of soysay](img/soysay.png)

*Example of soysay output*

### Piping / redirecting to soysay

```sh
echo "The current shell is $SHELL" | soysay
echo "soysay is located at: `which soysay`" | soysay
```

You can get soyjak to explain what soysay does:
```sh
cat `which soysay` | soysay
```

You can use bash redirection to send only the stderr stream to soysay, so normal output shows up normally and errors are said by soyjaks. (This is the intended use of soysay.)

```sh
python -c 'print("Hello!"); nonexistent_function()' 2> >(soysay)
```

This will print "Hello!" to the standard output, but soyjak will tell you that `nonexistent_function` is not defined.

For example, the following bash alias might be helpful:
```sh
alias make="make 2> >(soysay)"
```
