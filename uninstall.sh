#!/bin/bash

RED="\e[31m"
GREEN="\e[32m"
ENDCOLOR="\e[0m"

if ! which soysay >/dev/null
then
	echo -e "${RED}No soysay installation detected, you probably already deleted it${ENDCOLOR}"
	exit 1
fi

echo "Sorry you didn't like soysay!"
echo ""
echo "Uninstalling soysay..."

rm /usr/local/bin/soysay*
rm /usr/share/cows/*soyjak.cow

echo -e "${GREEN}soysay has been uninstalled.  Goodbye!${ENDCOLOR}"
